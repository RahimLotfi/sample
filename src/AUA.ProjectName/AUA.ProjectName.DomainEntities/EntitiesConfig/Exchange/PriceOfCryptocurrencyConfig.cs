﻿using AUA.ProjectName.DomainEntities.Entities.Exchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AUA.ProjectName.DomainEntities.EntitiesConfig.Exchange
{
    public class PriceOfCryptocurrencyConfig : IEntityTypeConfiguration<PriceOfCryptocurrency>
    {
        public void Configure(EntityTypeBuilder<PriceOfCryptocurrency> builder)
        {
            builder
                .HasOne(p => p.Cryptocurrency)
                .WithMany(p => p.PricesOfCryptocurrency)
                .HasForeignKey(p => p.CryptocurrencyId)
                .OnDelete(DeleteBehavior.ClientNoAction);

            builder
                .HasOne(p => p.Currency)
                .WithMany(p => p.PricesOfCryptocurrency)
                .HasForeignKey(p => p.CurrencyId)
                .OnDelete(DeleteBehavior.ClientNoAction);

            //builder
            //    .HasQueryFilter(p => p.IsActive);
        }

    }
}
