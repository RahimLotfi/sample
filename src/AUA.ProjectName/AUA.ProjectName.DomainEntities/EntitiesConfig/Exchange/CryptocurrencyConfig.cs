﻿using AUA.ProjectName.Common.Consts;
using AUA.ProjectName.DomainEntities.Entities.Exchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AUA.ProjectName.DomainEntities.EntitiesConfig.Exchange
{
    public class CryptocurrencyConfig : IEntityTypeConfiguration<Cryptocurrency>
    {
        public void Configure(EntityTypeBuilder<Cryptocurrency> builder)
        {

            builder
                .Property(p => p.Title)
                .HasMaxLength(LengthConsts.MaxStringLen50);

            //builder
            //    .HasQueryFilter(p => p.IsActive);
        }

    }
}
