﻿using AUA.ProjectName.DomainEntities.Tools.BaseEntities;

namespace AUA.ProjectName.DomainEntities.Entities.Exchange
{
    public class PriceOfCryptocurrency : DomainEntity<long>
    {
        public int CurrencyId { get; set; }

        public int CryptocurrencyId { get; set; }

        public decimal Price { get; set; }

        public Currency Currency { get; set; }

        public Cryptocurrency Cryptocurrency { get; set; }
    }
}
