﻿using AUA.ProjectName.DomainEntities.Tools.BaseEntities;

namespace AUA.ProjectName.DomainEntities.Entities.Exchange
{
    public class Cryptocurrency : DomainEntity<int>
    {
        public string Title { get; set; }

        public List<PriceOfCryptocurrency> PricesOfCryptocurrency { get; set; }
    }
}
