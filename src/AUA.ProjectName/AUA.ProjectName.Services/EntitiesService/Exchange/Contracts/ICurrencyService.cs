﻿using AUA.Infrastructure.BaseServices;
using AUA.ProjectName.DomainEntities.Entities.Exchange;

namespace AUA.ProjectName.Services.EntitiesService.Exchange.Contracts
{
    public interface ICurrencyService : IDomainEntityService<Currency, int>
    {

    }
}
