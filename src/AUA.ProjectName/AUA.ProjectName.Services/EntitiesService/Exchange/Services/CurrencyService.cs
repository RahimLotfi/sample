﻿using AUA.Infrastructure.BaseServices;
using AUA.ProjectName.DomainEntities.Entities.Exchange;
using AUA.ProjectName.Services.EntitiesService.Exchange.Contracts;

namespace AUA.ProjectName.Services.EntitiesService.Exchange.Services
{
    public class CurrencyService : DomainEntityService<Currency,int>, ICurrencyService
    {

    }
}
