﻿using AUA.ProjectName.Infrastructure.QueryInfra.Handler;
using AUA.ProjectName.Models.BaseModel.BaseViewModels;
using AUA.ProjectName.Queries.Queries.Exchange.PriceOfCryptocurrencyContracts;
using AUA.ProjectName.Services.EntitiesService.Exchange.Contracts;
using Microsoft.EntityFrameworkCore;

namespace AUA.ProjectName.QueryHandler.QueryHandler.Exchange.PriceOfCryptocurrencyHandlers
{
    public class PriceOfCryptocurrencyQueryHandler : BaseQueryHandler<PriceOfCryptocurrencyQuery, PriceOfCryptocurrencyQueryResponse>
    {
        private readonly IPriceOfCryptocurrencyService _priceOfCryptocurrencyService;

        public PriceOfCryptocurrencyQueryHandler(IPriceOfCryptocurrencyService priceOfCryptocurrencyService)
        {
            _priceOfCryptocurrencyService = priceOfCryptocurrencyService;
        }

        public override void Validation()
        {
            //if we have andy validations 
        }

        public override async Task<ResultModel<IEnumerable<PriceOfCryptocurrencyQueryResponse>>> ExecuteAsync(CancellationToken cancellationToken = new CancellationToken())
        {

            var model = await _priceOfCryptocurrencyService.GetAll()
                .OrderByDescending(p => p.Id)
                .FirstOrDefaultAsync(p => p.CryptocurrencyId == _request.CryptocurrencyId, cancellationToken);

            var result = new PriceOfCryptocurrencyQueryResponse
            {
                Price = model!.Price
            };

            return CreateSuccessResult(result);
        }

    }
}
