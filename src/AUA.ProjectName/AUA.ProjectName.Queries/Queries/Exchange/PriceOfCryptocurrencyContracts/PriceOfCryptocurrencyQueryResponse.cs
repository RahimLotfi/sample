﻿namespace AUA.ProjectName.Queries.Queries.Exchange.PriceOfCryptocurrencyContracts
{
    public class PriceOfCryptocurrencyQueryResponse
    {
        public decimal Price { get; set; }
    }
}
