﻿using AUA.Infrastructure.QueryInfra.Query;

namespace AUA.ProjectName.Queries.Queries.Exchange.PriceOfCryptocurrencyContracts
{
    public class PriceOfCryptocurrencyQuery : BaseQuery<PriceOfCryptocurrencyQueryResponse>
    {
        public int CryptocurrencyId { get; set; }
    }
}
