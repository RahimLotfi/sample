﻿using AUA.ProjectName.Common.Consts;
using AUA.ProjectName.DomainEntities.Entities.Exchange;
using AUA.ProjectName.Models.BaseModel.BaseViewModels;
using AUA.ProjectName.Queries.Queries.Accounting.UserContacts;
using AUA.ProjectName.Queries.Queries.Exchange.PriceOfCryptocurrencyContracts;
using AUA.ProjectName.WebApi.Controllers;
using AUA.ProjectName.WebApi.Utility.ApiAuthorization;
using Microsoft.AspNetCore.Mvc;

namespace AUA.ProjectName.WebApi.Areas.Exchange;


[ApiVersion(ApiVersionConsts.V1)]
[AllowAnonymousAuthorize]
public class ExchangeController : BaseApiController
{

    [HttpGet]
    public async Task<ResultModel<IEnumerable<PriceOfCryptocurrencyQueryResponse>>> GetUserContacts(int cryptocurrencyId)
    {
        var request = new PriceOfCryptocurrencyQuery
        {
            CryptocurrencyId = cryptocurrencyId
        };

        return await RequestDispatcher.Send(request);
    }




}